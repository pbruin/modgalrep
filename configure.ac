# -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.68])
AC_INIT([modgalrep], [0.2], [P.J.Bruin@math.leidenuniv.nl])

AM_INIT_AUTOMAKE([-Wall -Werror foreign])

AC_CONFIG_SRCDIR([modgalrep.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

# Checks for programs.
AC_PROG_CC

# Libtool support.
AM_PROG_AR
LT_INIT

eval module=no shrext=$shrext_cmds
AC_SUBST([shrext])

AC_ARG_WITH([pari],
  [AS_HELP_STRING([--with-pari=DIR],
    [use PARI/GP headers and libraries in DIR])],
  [if test -n "$with_pari"; then
     with_pari_include="$with_pari/include"
     with_pari_lib="$with_pari/lib"
   fi],
  [])

AC_ARG_WITH([pari-include],
  [AS_HELP_STRING([--with-pari-include=DIR],
    [use PARI/GP headers in DIR])],
  [], [])

AC_ARG_WITH([pari-lib],
  [AS_HELP_STRING([--with-pari-lib=DIR],
    [use PARI/GP libraries in DIR])],
  [], [])

if test -n "$with_pari_include"; then
  CPPFLAGS="$CPPFLAGS -I$with_pari_include"
fi

if test -n "$with_pari_lib"; then
  LDFLAGS="$LDFLAGS -L$with_pari_lib -Wl,-rpath -Wl,$with_pari_lib"
fi

# Checks for libraries.
AC_CHECK_LIB([m], [log])
AC_CHECK_LIB([pari], [pari_init])
AC_CHECK_LIB([argp], [argp_parse])

# Checks for header files.
AC_CHECK_HEADERS([stdlib.h string.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_INLINE
AC_TYPE_SIZE_T

# Checks for library functions.
AC_CHECK_FUNCS([pow sqrt strtoul RgX_to_FlxqX])

AC_CONFIG_FILES([Makefile
                 algebra/Makefile
                 jacobian/Makefile
                 modcurve/Makefile
                 modular/Makefile])

AC_OUTPUT
