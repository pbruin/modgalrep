#ifndef MODULAR_JACOBIAN
#define MODULAR_JACOBIAN

GEN modular_curve_jacobian_medium(GEN group, ulong p, GEN embedding_forms);
GEN modular_curve_jacobian_large(GEN group, ulong p, GEN embedding_forms);
GEN modular_jacobian_normalised_divisor (GEN X, GEN W_D, GEN W_rO);

#endif
