#include <pari/pari.h>
#include <pari/paripriv.h>  /* ucoeff */

#include "curve.h"
#include "divisor.h"
#include "finite-fields.h"
#include "jacobian.h"
#include "l-torsion.h"
#include "matsmall.h"
#include "modular-curve.h"
#include "modular-jacobian.h"
#include "pairings.h"
#include "torsion-subscheme.h"


#define GIVEUP 1  /* give up if computation is too hard */


/*
  Simple-minded function to compute the multiplicative order
  of  x  modulo  f.
*/
static unsigned long
order_mod(GEN x, GEN f) {
  unsigned long k;
  pari_sp av = avma;

  if(degpol(RgX_gcd(x, f)) != 0)
    pari_err(e_MISC, "%Ps not invertible modulo %Ps", x, f);
  for(k = 1; !gequal1(gpowgs(gmodulo(x, f), k)); k++);
  avma = av;
  return k;
}

/*
  Factor the polynomial F  as

    F = f_primary * projector  in  F_l[x],

  where  f_primary  has the same irreducible factors as  f
  and  projector  is coprime to  f.
*/
static void
factor_coprime(GEN F, GEN f, GEN *f_primary, GEN *projector, long l) {
  GEN g;
  GEN factorisation = factormod0(F, stoi(l), 0);
  long i;

  *f_primary = *projector = gen_1;
  for(i = 1; i < lg(gel(factorisation, 1)); i++) {
    g = gpowgs(gcoeff(factorisation, i, 1),
	       itos(gcoeff(factorisation, i, 2)));
    if(gequal0(gmod(f, gcoeff(factorisation, i, 1))))
      *f_primary = gmul(*f_primary, g);
    else
      *projector = gmul(*projector, g);
  }
}

/*
  Check whether f_primary cuts out the representation.  First try to
  deduce this from the degree of f_primary; if this fails, count
  points to see if the subspace is only two-dimensional.
*/
static void
check_multiplicity(GEN group, long l, long p, GEN f_primary,
		   long extension_degree) {
  GEN numzeta, N;
  long d = degpol(f_primary);
  if (d == 2)
    return;
  if (d < 2)
    pari_err(e_MISC, "subspace does not occur in the Jacobian");
  numzeta = modular_curve_numerator_zeta_function(group, p);
  numzeta = numzeta_base_change(numzeta, extension_degree);
  N = gsubst(numzeta, varn(numzeta), gen_1);
  if (gvaluation(N, stoi(l)) != 2)
    pari_err(e_MISC, "subspace not determined by Frobenius action");
}

static GEN
find_extension(GEN group, unsigned long l, unsigned long p,
	       GEN f, GEN *f_dual, GEN *projector, GEN *projector_dual,
	       unsigned long max_degree) {
  pari_sp av = avma;
  GEN charpoly_Frob = modular_curve_characteristic_polynomial_frobenius(group, p);
  GEN f_primary, extension, x;
  long extension_degree;

  x = pol_x(gvar(f));
  extension_degree = order_mod(x, f);
  extension_degree = clcm(extension_degree, Fl_order(p, 0, l));

  if (extension_degree > max_degree)
    pari_err(e_MISC, "extension degree too high (%li)", extension_degree);

  err_printf("using extension of F_%li of degree %li\n", p, extension_degree);
  extension = finite_field_extension(extension_degree, p, NULL);

  /* f = x^2 + a*x + b  ==>  g = x^2 + (p*a/b)*x + p^2/b */
  *f_dual = shallowcopy(f);
  gel(*f_dual, 3) = gmulsg(p, gdiv(gel(f, 3), gel(f, 2)));
  gel(*f_dual, 2) = gdiv(sqru(p), gel(f, 2));

  factor_coprime(charpoly_Frob, f, &f_primary, projector, l);
  check_multiplicity(group, l, p, f_primary, extension_degree);

  err_printf("characteristic polynomial of Frobenius: %Ps\n", f);
  if(gequal(*f_dual, f))
    *projector_dual = *projector;
  else {
    err_printf("characteristic polynomial of Frobenius for the"
	       " dual representation: %Ps\n", *f_dual);
    factor_coprime(charpoly_Frob, *f_dual, &f_primary, projector_dual, l);
    check_multiplicity(group, l, p, f_primary, extension_degree);
  }

  gerepileall(av, 4, &extension, f_dual, projector, projector_dual);
  return extension;
}

/* Naïve algorithm for finding a random  l-torsion point.  */
static GEN
random_torsion_point(GEN J, unsigned long l, int *tries) {
  GEN n, m;
  long v;
  GEN P, Q;
  pari_sp av = avma;

  n = jacobian_count_points(J);
  v = gvaluation(n, stoi(l));
  if(v == 0)
    pari_err(e_MISC, "no point of order %li", l);
  m = gdiv(n, powuu(l, v));
  while((*tries)-- > 0) {
    err_printf("finding random point\n");
    P = jacobian_random_point(J);
    err_printf("multiplying by %Ps\n", m);
    P = jacobian_multiply(J, P, m);
    if(!jacobian_is_zero(J, P)) {
      while(1) {
	Q = jacobian_multiply(J, P, stoi(l));
	if(jacobian_is_zero(J, Q))
	  break;
	P = Q;
      }
      break;
    }
  }
  return gerepileupto(av, P);
}

static GEN
multiples(GEN J, GEN P, long l) {
  GEN mult = cgetg(l + 1, t_VEC);
  long i;
  gel(mult, 1) = jacobian_zero(J);
  gel(mult, 2) = P;
  for(i = 2; i < l; i++)
    gel(mult, i + 1) = jacobian_add(J, gel(mult, i), P);
  return mult;
}

static int
point_index(GEN J, GEN V, GEN P) {
  long i;
  for (i = 1; i < lg(V); i++) {
    if (jacobian_equal(J, gel(V, i), P))
      return i;
  }
  return 0;
}

/*
  Naïve algorithm for finding bases of the subspaces of the
  l-torsion defined by the elements of  proj.
*/
static void
find_bases(GEN J, unsigned long l, GEN f, GEN proj, int tries,
	   GEN *bases, GEN *matrices) {
  GEN P, Q, R, V1, V2, M;
  long i, j, k, n = lg(proj) - 1, found = 0, d;
  pari_sp av = avma;

  *bases = cgetg(n + 1, t_VEC);
  *matrices = cgetg(n + 1, t_VEC);
  for (i = 1; i <= n; i++)
    gel(*bases, i) = cgetg(1, t_VEC);
  while (tries > 0 && found < 2*n) {
    P = random_torsion_point(J, l, &tries);
    for(i = 1; i <= n; i++) {
      j = lg(gel(*bases, i)) - 1;
      if (j == 2)
	continue;
      err_printf("applying Frobenius polynomial %Ps\n", gel(proj, i));
      Q = jacobian_Frob_polynomial(J, P, centerlift(gel(proj, i)), 1);
      if (jacobian_is_zero(J, Q))
	continue;
      if (j == 0) {
	V1 = multiples(J, Q, l);
	R = jacobian_Frob(J, Q, 1);
	if ((k = point_index(J, V1, R)) != 0) {
	  gel(*bases, i) = mkvec(V1);
	  gel(*matrices, i) = mkmat2(mkvecsmall2(k - 1, 0), zero_Flv(2));
	  found++;
	} else {
	  V2 = multiples(J, R, l);
	  gel(*bases, i) = mkvec2(V1, V2);
	  gel(*matrices, i) = RgM_to_Flm(matcompanion(gel(f, i)), l);
	  found += 2;
	}
      } else {
	V1 = gmael(*bases, i, 1);
	if ((k = point_index(J, V1, Q)) == 0) {
	  V2 = multiples(J, Q, l);
	  gel(*bases, i) = mkvec2(V1, V2);
	  /* with S = V1[1]:  Frob(S) = a*S,  Frob(Q) = b*S + d*Q */
	  M = gel(*matrices, i);
	  d = Fl_neg(Fl_add(ucoeff(M, 1, 1),
			    Rg_to_Fl(gmael(f, i, 3), l), l), l);
	  ucoeff(M, 2, 2) = d;
	  R = jacobian_subtract(J, jacobian_Frob(J, Q, 1),
				jacobian_multiply(J, Q, centerlift(gmodulss(d, l))));
	  if ((k = point_index(J, V1, R)) == 0)
	    pari_err(e_MISC, "inconsistent Frobenius action");
	  ucoeff(M, 1, 2) = k - 1;
	  found++;
	}
      }
    }
    if (gc_needed(av, 1))
      gerepileall(av, 2, bases, matrices);
  }
  if (found < 2*n) {
    if (GIVEUP)
      pari_err(e_MISC, "too many tries to find bases");
    *bases = *matrices = NULL;
  }
  err_printf("Frobenius matrices: %Ps\n", *matrices);
}

static GEN
eval_function(GEN J, GEN D, GEN multiples_O, long *w) {
  unsigned long p = curve_base_field_characteristic (J);
  GEN T = curve_base_field_polynomial (J);
  GEN R, x;
  long a, i, r;
  pari_sp av = avma;

  D = modular_jacobian_normalised_divisor(J, D, multiples_O);
  D = matsmall_echelon(D, &R, p, T);
  r = lg(D) - 1;
  /* compute the "weight" */
  *w = 0;
  for (i = 1; i <= r; i++)
    *w += R[i];

  if (T == NULL) {
    a = coeff(D, R[r] + 1, r);
    x = gmodulss(a, p);
  } else {
    x = gcoeff(D, R[r] + 1, r);
    x = gmodulo(gmul(Flx_to_ZX(x), gmodulss(1, p)),
		gmul(Flx_to_ZX(T), gmodulss(1, p)));
  }
  return gerepileupto(av, x);
}

static GEN
values_from_basis(GEN J, GEN V1, GEN V2, long l, GEN matrix,
		  GEN multiples_O) {
  unsigned long p = curve_base_field_characteristic(J);
  pari_sp av = avma;
  long i, j, w, W = 0;
  GEN D, V, v, a;

  err_printf("computing function values:");
  V = zeromatcopy(l, l);
  for (i = 0; i < l; i++) {
    for (j = 0; j < l; j++) {
      if (!isintzero(gcoeff(V, i + 1, j + 1)))
	continue;
      err_printf(" (%li, %li)", i, j);
      err_flush();
      D = jacobian_addflip(J, gel(V1, i + 1), gel(V2, j + 1));
      a = eval_function(J, D, multiples_O, &w);
      v = mkvecsmall2(i, j);
      while (1) {
	gcoeff(V, v[1] + 1, v[2] + 1) = a;
	W += w;
	v = Flm_Flc_mul(matrix, v, l);
	if (!isintzero(gcoeff(V, v[1] + 1, v[2] + 1)))
	  break;
	a = gpowgs(a, p);
      }
      if (gc_needed(av, 1))
	V = gerepilecopy(av, V);
    }
  }
  err_printf("\n");
  return gerepilecopy(av, mkvec2(V, stoi(W)));
}

/*
  Return either [V, W, Z] or [V, W, V_dual, W_dual, Z], where
  V  is the matrix of all function values,  W  is the total
  weight, similarly for  V_dual  and  W_dual, and  Z  encodes
  the Weil pairing on  V  (resp. between  V  and  V_dual).
*/
static GEN
all_function_values(GEN J, unsigned long l, GEN f, GEN proj, int tries) {
  pari_sp av = avma;
  GEN bases, matrices, basis, basis_dual;
  GEN matrix, matrix_dual, values, values_dual;
  GEN O, multiples_O, V1, V2, V1_dual, V2_dual;
  GEN P, Q, P_dual, Q_dual, z, Z;

  err_printf("computing multiples of the distinguished point\n");
  O = modular_curve_distinguished_point(J);
  multiples_O = curve_point_multiples(J, O);

  err_printf("computing bases for the desired subspaces of the %li-torsion\n", l);
  find_bases(J, l, f, proj, tries, &bases, &matrices);
  if (bases == NULL) {
    basis = jacobian_l_torsion_basis(J, l, multiples_O);
    /* TODO: project */
  }

  basis = gel(bases, 1);
  V1 = gel(basis, 1);
  V2 = gel(basis, 2);
  P = gel(V1, 2);
  Q = gel(V2, 2);
  matrix = gel(matrices, 1);
  values = values_from_basis(J, V1, V2, l, matrix, multiples_O);

  if (lg(proj) == 2) {
    err_printf("computing Weil pairing\n");
    z = jacobian_weil_pairing(J, P, Q, l);
    if(gequal1(z) || !gequal1(gpowgs(z, l)))
      pari_err(e_MISC, "Weil pairing not of order %li", l);
    return gerepilecopy(av, shallowconcat(values, mkvec(z)));
  }
  else {
    basis_dual = gel(bases, 2);
    V1_dual = gel(basis_dual, 1);
    V2_dual = gel(basis_dual, 2);
    P_dual = gel(V1_dual, 2);
    Q_dual = gel(V2_dual, 2);
    matrix_dual = gel(matrices, 2);
    values_dual = values_from_basis(J, V1_dual, V2_dual, l, matrix_dual,
				    multiples_O);
    err_printf("computing Weil pairings\n");
    Z = mkmat2(mkcol2(jacobian_weil_pairing(J, P, P_dual, l),
		      jacobian_weil_pairing(J, Q, P_dual, l)),
	       mkcol2(jacobian_weil_pairing(J, P, Q_dual, l),
		      jacobian_weil_pairing(J, Q, Q_dual, l)));
    return gerepilecopy(av, shallowconcat1(mkvec3(values, values_dual,
						  mkvec(Z))));
  }
}

/*
  Return the torsion subscheme of J_Gamma over F_p realising the mod l
  representation attached to a cusp form such that the characteristic
  polynomial of Frob_p equals f.

  We check that this subscheme is two-dimensional over F_l.
*/
GEN
torsion_subscheme(GEN Gamma, unsigned long l, unsigned long p,
		  GEN f, unsigned long max_degree, unsigned long tries) {
  GEN f_dual, projector, projector_dual, proj;
  GEN extension, J, J_k, order_J, values;
  long v;
  pari_sp av = avma;

  /* Find the field of definition of the representation space.  */
  extension = find_extension(Gamma, l, p, f, &f_dual, &projector,
			     &projector_dual, max_degree);

  /*
    Compute the Jacobian and its base change to the right field.
  */
  J = modular_curve_jacobian_medium(Gamma, p, gp_read_file("embedding_forms.gp"));
  err_printf("working with curve of genus %li with projective"
	     " embedding of degree %li into P^%li\n",
	     curve_genus(J), curve_degree(J), lg(curve_V(J, 1)) - 2);

  J_k = jacobian_base_change(J, extension);

  order_J = jacobian_count_points(J_k);
  v = gvaluation(order_J, stoi(l));
  pari_printf("Jacobian has order %Pi = %li^%li * %Pi\n",
	      order_J, l, v, gdiv(order_J, powuu(l, v)));

  if (RgX_equal(f, f_dual)) {
    f = mkvec(f);
    proj = mkvec(projector);
  } else {
    f = mkvec2(f, f_dual);
    proj = mkvec2(projector, projector_dual);
  }
  values = all_function_values(J_k, l, f, proj, tries);
  return gerepileupto(av, values);
}
