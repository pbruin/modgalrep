#ifndef MODULAR_CURVE
#define MODULAR_CURVE

GEN modular_curve_characteristic_polynomial_frobenius(GEN group, unsigned long p);
GEN modular_curve_numerator_zeta_function(GEN group, unsigned long p);
GEN modular_curve_zeta_function(GEN group, unsigned long p);
GEN modular_curve_count_points(GEN group, unsigned long p, unsigned long k);
GEN modular_curve_jacobian_count_points(GEN group, unsigned long p,
					unsigned long k);
GEN modular_curve(GEN group, ulong p, ulong max_power, GEN embedding_forms);
GEN modular_curve_distinguished_point(GEN X);

#endif
