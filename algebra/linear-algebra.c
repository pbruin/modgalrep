/***
    
    Various linear algebra functions

***/

#include <pari/pari.h>

#include "linear-algebra.h"

GEN
coker (GEN A) {
  pari_sp ltop = avma;
  return gerepilecopy(ltop, shallowtrans(ker(shallowtrans(A))));
}

/*
  Given an  m \times n  matrix  A  over a field  K  and a subspace
  V  of  K^m,  this function computes the subspace of  K^n  consisting
  of all vectors  v  for which  Av  is contained in  V.
*/
GEN
mat_inverse_image (GEN A, GEN V) {
  pari_sp ltop = avma;
  return gerepileupto (ltop, ker (gmul (coker (V), A)));
}

/*
  Given an  m \times n  matrix  A  over a field  K,  viewed as
  a map  K^n -> K^m,  this function computes the cokernel of  A
  as a pair  [P, B],  where  P  is a vector of indices of a basis
  of  K^m  forming a basis for  coker(A)  and  B  is the matrix
  of the quotient map with respect to the standard basis of  K^m
  and the basis of  coker(A)  given by  P.
*/

GEN
coker_basis_and_map (GEN A) {
  pari_sp ltop = avma;
  /* Compute an upper triangular column echelon form of  A.  */
  A = gel (reduced_column_echelon_form (A, NULL, 1), 2);
  return gerepileupto (ltop, quotient_upper_triangular (A));
}

/*
  Given an upper triangular  m \times n  matrix  A  over a field  K,
  viewed as a map  K^n -> K^m,  this function computes the cokernel
  of  A  as a pair  [P, B],  where  P  is a vector of indices of
  a basis of  K^m  forming a basis for  coker(A)  and  B  is the
  matrix of the quotient map with respect to the standard basis
  of  K^m  and the basis of  coker(A)  given by  P.
*/

GEN
quotient_upper_triangular (GEN A) {
  long columns = lg (A) - 1;
  long rows;
  long quotient_rank;
  GEN which_pivot, pivots;
  long i, j, k;
  GEN map;
  pari_sp ltop = avma;

  if (columns == 0)
    rows = 0;
  else
    rows = lg (gel (A, 1)) - 1;
  quotient_rank = rows - columns;
  which_pivot = cgetg (rows + 1, t_VECSMALL);
  for (i = 1; i <= rows; i++)
    which_pivot[i] = 0;
  pivots = cgetg (quotient_rank + 1, t_VECSMALL);
  for (i = rows, j = 0, k = quotient_rank;
       j < columns;
       j++, i--)
    while (i > 0 && gcmp0 (gcoeff (A, i, columns - j))) {
      which_pivot[i] = k;
      pivots[k] = i;
      k--;
      i--;
    }
  while (i > 0) {
    which_pivot[i] = k;
    pivots[k] = i;
    k--;
    i--;
  }

  /* Warning: zeromat sets all columns of MAP to the _same_ zero
     vector, so we cannot use gcoeff as an lvalue below.  (An
     alternative would be to use zeromatcopy.)  */
  map = zeromat (quotient_rank, rows);
  for (i = 1, j = 0; i <= rows; i++) {
    if (which_pivot[i] > 0) {
      gel (map, i) = col_ei (quotient_rank, which_pivot[i]);
    } else {
      j++;
      if (gcmp1 (gcoeff (A, i, j))) {
	gel (map, i) = gneg (gmul (map, gel (A, j)));
      } else {
	/* pari_warn (warner, "pivot element [%li, %li] = %Z not equal to 1", i, j, gcoeff (A, i, j)); */
	gel (map, i) = gmul (gdiv (gen_m1, gcoeff (A, i, j)),
			     gmul (map, gel (A, j)));
      }
    }
  }
  return gerepilecopy (ltop, mkvec2 (pivots, map));
}


/*
  Bring the matrix  A  into lower triangular reduced column echelon
  form.  If  B != NULL,  apply the same transformations to  B.
  If  UPPER_TRIANGULAR  is non-zero, compute the upper triangular
  reduced column echelon form instead.  Call the results  A'  and  B',
  respectively.
  If  B == NULL,  return  [rank, A'];
  if  B != NULL,  return  [rank, A', B'].
*/
GEN
reduced_column_echelon_form (GEN A, GEN B, long upper_triangular) {
  long rows, columns;
  long i, j, pivot;
  long rank;
  pari_sp ltop;
  pari_sp stack_limit;

  columns = lg (A) - 1;
  if (B != NULL && columns != lg (B) - 1)
    pari_err (e_MISC, "matrices don't have the same number of columns");
  if (columns == 0) {
    GEN result = cgetg (4, t_VEC);
    gel (result, 1) = gen_0;
    gel (result, 2) = cgetg (1, t_MAT);
    gel (result, 3) = cgetg (1, t_MAT);
    return result;
  }
  ltop = avma;
  stack_limit = stack_lim (ltop, 1);
  A = shallowcopy (A);
  if (B != NULL)
    B = shallowcopy (B);
  rows = lg (gel (A, 1)) - 1;
  pivot = upper_triangular ? columns + 1 : 0;
  for (i = upper_triangular ? rows : 1;
       upper_triangular ? i > 0 : i <= rows;
       upper_triangular ? i-- : i++) {
    for (j = upper_triangular ? pivot - 1 : pivot + 1;
	 upper_triangular ? j > 0 : j <= columns;
	 upper_triangular ? j-- : j++) {
      GEN c;
      int j1;
      if (!gcmp0 (gcoeff (A, i, j))) {
	if (upper_triangular)
	  pivot--;
	else
	  pivot++;
	if (j != pivot) {
	  /* swap columns J and PIVOT */
	  GEN v = gel (A, j);
	  gel (A, j) = gel (A, pivot);
	  gel (A, pivot) = v;
	  if (B != NULL) {
	    v = gel (B, j);
	    gel (B, j) = gel (B, pivot);
	    gel (B, pivot) = v;
	  }
	}
	/* divide by the pivot element */
	c = gcoeff (A, i, pivot);
	if (gcmp0 (c))
	  pari_err (e_MISC, "pivot %Z (type %i) equals zero?!", c, typ (c));
	gel (A, pivot) = gdiv (gel (A, pivot), c);
	if (B != NULL)
	  gel (B, pivot) = gdiv (gel (B, pivot), c);
	/* clear the I-th row */
	for (j1 = 1; j1 <= columns; j1++)
	  if (j1 != pivot) {
	    c = gcoeff (A, i, j1);
	    gel (A, j1) = gsub (gel (A, j1),
				gmul (c, gel (A, pivot)));
	    if (B != NULL)
	      gel (B, j1) = gsub (gel (B, j1),
				  gmul (c, gel (B, pivot)));
	  }
	break;
      }
    }
    if (avma < stack_limit) {
      if (DEBUGMEM > 1)
	pari_warn (warnmem, "reduced_column_echelon_form");
      if (B == NULL)
	A = gerepilecopy (ltop, A);
      else
	gerepileall (ltop, 2, &A, &B);
    }
  }
  rank = upper_triangular ? columns + 1 - pivot : pivot;
  if (B == NULL)
    return gerepilecopy (ltop, mkvec2 (stoi (rank), A));
  else
    return gerepilecopy (ltop, mkvec3 (stoi (rank), A, B));
}
