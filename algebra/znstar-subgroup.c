#include <pari/pari.h>

#include "znstar-subgroup.h"

static GEN
ZM_split(GEN A) {
  GEN U;
  long r = lg(A) - 1, s;
  if (r == 0)
    return cgetg(1, t_MAT);
  s = nbrows(A);
  U = gel(hnfall(A), 2);
  if (r != s)
    U = vecslice(U, r - s + 1, r);
  return U;
}

/*
  G  is znstar(n, 1)
  H  is a subgroup of  G  given by its HNF matrix
     relative to the SNF generators of  G
  Return an Abelian group structure [no, cyc, gen] corresponding to  H.
*/
GEN
znstar_subgroup(GEN G, GEN H) {
  pari_sp av = avma;
  GEN n = member_mod(G),
    cyc = member_cyc(G),
    gen = member_gen(G),
    D = diagonal(cyc),
    M = ZM_gauss(H, D),
    S = matsnf0(M, 5),
    U = gel(S, 1),
    E = gel(S, 3),
    B = ZM_mul(H, ZM_split(U));
  long i, j, r = lg(cyc) - 1, s = lg(E) - 1;
  settyp(E, t_VEC);
  for (i = 1; i <= s; i++)
    gel(E, i) = gmael(E, i, i);
  settyp(B, t_VEC);
  for (i = 1; i <= s; i++) {
    for (j = 1; j <= r; j++)
      gmael(B, i, j) = modii(gmael(B, i, j), gel(cyc, j));
    gel(B, i) = gmodulo(factorback2(gen, gel(B, i)), n);
  }
  return gerepilecopy(av, mkvec3(vecprod(E), E, B));
}
