GEN Flxq_to_FF(GEN x, ulong p, GEN T);

GEN finite_field_extension (long n, unsigned long p, GEN T);
GEN finite_field_vector_up (GEN x, GEN extension);
GEN finite_field_vector_down (GEN x, GEN extension);
GEN finite_field_vector_to_prime_field (GEN x, unsigned long p);
GEN finite_field_matrix_up (GEN x, GEN extension);
GEN finite_field_matrix_down (GEN x, GEN extension);
GEN finite_field_matrix_to_prime_field (GEN x, unsigned long p);
