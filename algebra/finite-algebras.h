GEN maximal_ideal_local (GEN A);
GEN primary_decomposition (GEN A);
GEN maximal_ideals (GEN A);
GEN bilinear_map_to_algebra (GEN mu);
