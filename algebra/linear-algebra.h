GEN mat_inverse_image (GEN A, GEN V);
GEN coker (GEN A);
GEN coker_basis_and_map (GEN A);
GEN quotient_upper_triangular (GEN A);
GEN reduced_column_echelon_form (GEN A, GEN B, long upper_triangular);
