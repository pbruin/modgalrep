modgalrep
=========

A program to compute Galois representations attached to modular forms
over finite fields.


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

- PARI/GP 2.10.0.alpha or later, <http://pari.math.u-bordeaux.fr/>.

- To install from the Git repository: GNU autotools.


Installation
------------

If you obtained this package from the Git repository, first install
Autoconf files:

    $ mkdir -p m4; autoreconf -is

Then build and install the package (if ``configure`` cannot find
PARI/GP headers and libraries, add ``--with-pari=<paridir>``, with
``<paridir>`` replaced by the correct directory):

    $ ./configure
    $ make
    $ make install

Finally, add the directory where the GP files were installed (by
default ``/usr/local/share/modgalrep``) to the variable ``path`` in
your ``.gprc`` file (this step is optional).


Usage
-----

Here is an example:

    $ cd examples/level_11_weight_2_mod_2
    $ make

After ``make`` finishes, the file ``summary.txt`` will contain a
summary of the data computed.


TODO
----

- Better documentation
