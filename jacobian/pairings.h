#ifndef PAIRINGS
#define PAIRINGS

GEN jacobian_tate_pairing (GEN J, GEN W_D, GEN W_E, long n);
GEN jacobian_weil_pairing(GEN J, GEN W_D, GEN W_E, long n);

#endif
