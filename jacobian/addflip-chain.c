#include <pari/pari.h>

#include "addflip-chain.h"

/*
  Generate an addflip chain for  n, i.e. a vector
    [[i_2, j_2], [i_3, j_3] ..., [i_m, j_m]]
  such that the sequence
    a_0, a_1, a_2, ..., a_m
  with  a_0 = 0;
        a_1 = 1;
        a_l = -a_{i_l} - a_{j_l}  for l >= 2
  satisfies a_m = n.
*/

/*** Approach 1: binary powering ***/
GEN
addflip_chain_binpow(GEN n)
{
  GEN c, v;
  pari_sp av = avma;

  if (gequal0(n))
    v = mkvec(mkvecsmall2(0, 0));
  else if (gequal1(n))
    v = cgetg(1, t_VEC);
  else if (gequalm1(n))
    v = mkvec(mkvecsmall2(0, 1));
  else if (gequal0(gmodgs(n, 2))) {
    c = addflip_chain_binpow(gneg(gdivgs(n, 2)));
    v = shallowconcat(c, mkvec(mkvecsmall2(glength(c) + 1, glength(c) + 1)));
  }
  else {
    /* n % 2 == 1 */
    c = addflip_chain_binpow(gneg(gaddgs(n, 1)));
    v = shallowconcat(c, mkvec(mkvecsmall2(1, glength(c) + 1)));
  }
  return gerepilecopy(av, v);
}

/*** Approach 2: precomputed sequences ***/

static GEN precomputed_seqs = NULL;
static GEN precomputed_min, precomputed_max, precomputed_range;

static GEN
get_precomputed_seqs (GEN n)
{
  GEN c;
  long i;
  for (i = 1; i < lg(precomputed_seqs); i++) {
    c = gel(precomputed_seqs, i);
    if (gequal(gel(c, 1), n))
      return gel(c, 2);
  }
  return gnil;
}

/* Return 1 if and only if x is not in c = (t_VEC) E.  */
static long
does_not_contain(void *E, GEN x)
{
  GEN c = (GEN) E;
  long i;
  for (i = 1; i < lg(c); i++)
    if (gequal(gel(c, i), x))
      return 0;
  return 1;
}

/* Concatenate c and d, removing duplicates.  */
static GEN
vec_merge(GEN c, GEN d)
{
  return shallowconcat(c, vecselect(c, does_not_contain, d));
}

static GEN
addflip_seq(GEN n)
{
  GEN q, r, m, C, Cm, Cr, Cend, Cnew;
  long i, v;
  pari_sp av;
  if (precomputed_seqs == NULL) {
    av = avma;
    precomputed_seqs = gclone(gp_read_file("addflip-seq.gp"));
    precomputed_min = gmael(precomputed_seqs, 1, 1);
    precomputed_max = gmael(precomputed_seqs, lg(precomputed_seqs) - 1, 1);
    precomputed_range = gclone(gaddgs(gsub(precomputed_max, precomputed_min), 1));
    avma = av;
  }
  if (gcmp(n, precomputed_min) >= 0
      && gcmp(n, precomputed_max) <= 0)
    return gcopy(gel(get_precomputed_seqs(n), 1));
  av = avma;
  /* Write n = -q - r with r in the precomputed range.  */
  r = gadd(gmod(gneg(gadd(n, precomputed_min)), precomputed_range),
	   precomputed_min);
  q = gneg(gadd(n, r));
  /* Factor q = (-2)^v * m.  */
  v = gvaluation(q, gen_2);
  m = gdiv(q, gpowgs(gen_m2, v));
  /* Compute Cend = [-2*m, (-2)^2*m, ..., q].  */
  Cend = cgetg(v + 1, t_VEC);
  for (i = 1; i <= v; i++)
    gel(Cend, i) = gmul(gpowgs(gen_m2, i), m);
  C = gnil;
  Cm = addflip_seq(m);
  Cr = get_precomputed_seqs(r);
  for (i = 1; i < lg(Cr); i++) {
    Cnew = vec_merge(vec_merge(gel(Cr, i), Cm), Cend);
    if (!gequal(gel(Cnew, lg(Cnew) - 1), n))
      Cnew = shallowconcat(Cnew, mkvec(n));
    if (C == gnil || lg(Cnew) < lg(C))
      C = Cnew;
  }
  return gerepilecopy(av, C);
}

/* Compile a sequence into a chain.  */
GEN
compile_seq(GEN v)
{
  long l = glength(v);
  GEN c = cgetg(l - 1, t_VEC), m;
  long i, j, k;

  for (i = 1; i <= l - 2; i++) {
    pari_sp av = avma;
    GEN min_n = gneg(gel(v, i + 2));
    if (smodis(min_n, 2) == 0) {
      /* Try to use multiplication by -2 if possible.  */
      m = diviuexact(min_n, 2);
      for (j = 0; j <= i; j++) {
	if (gequal(m, gel(v, j + 1))) {
	  gel(c, i) = gerepileuptoleaf(av, mkvecsmall2(j, j));
	  goto OK;
	}
      }
    }
    for (j = 0; j <= i; j++) {
      for (k = 0; k <= i; k++) {
	if (gequal(min_n, gadd(gel(v, j + 1), gel(v, k + 1)))) {
	  gel(c, i) = gerepileuptoleaf(av, mkvecsmall2(j, k));
	  goto OK;
	}
      }
    }
    pari_err(e_MISC, "inconsistent addflip sequence");
  OK:
    continue;
  }
  return c;
}

/*
  TODO: this sometimes creates unnecessary steps in the chain, e.g.
  in addflip_chain(100) the 5th step is not used.
*/
GEN
addflip_chain(GEN n) {
  pari_sp av = avma;
  return gerepileupto(av, compile_seq(addflip_seq(n)));
}

/* Evaluate a chain into a sequence.  */
GEN
eval_chain(GEN c)
{
  long l = glength(c);
  GEN v = cgetg(l + 3, t_VEC);
  long i;

  gel(v, 1) = gen_0;
  gel(v, 2) = gen_1;
  for (i = 1; i <= l; i++) {
    pari_sp av = avma;
    gel(v, i + 2) = gerepileupto(av, gneg(gadd(gel(v, mael(c, i, 1) + 1),
					       gel(v, mael(c, i, 2) + 1))));
  }
  return v;
}


/*
  Example: addflip_chain(163)
  163 = -(1 - 164)	163 = -(-1 - 162)
  -164 = -(82 + 82)	-162 = -(81 + 81)
  82 = -(-41 - 41)	81 = -(-1 - 80)
  -41 = -(1 + 40)	-80 = -(40 + 40)
  40 = -(-20 - 20)
  -20 = -(10 + 10)
  10 = -(-5 - 5)
  -5 = -(1 + 4)
  4 = -(-2 - 2)
  -2 = -(1 + 1)
*/
