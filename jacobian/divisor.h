#ifndef DIVISOR
#define DIVISOR

GEN curve_divisor_to_point (GEN X, GEN P);
GEN curve_point_to_divisor (GEN X, GEN P);
GEN curve_point_multiples (GEN X, GEN P);
GEN curve_divisor_IGS (GEN X, GEN W_D, unsigned long i);
GEN curve_divisor_algebra (GEN X, GEN W_D, GEN W_2_D,
			   unsigned long d);
GEN curve_decompose_divisor (GEN X, GEN W_D, GEN W_2_D,
			     unsigned long d);
GEN curve_random_prime_divisor (GEN X, unsigned long n,
				unsigned long d);
GEN curve_random_point (GEN X);
GEN curve_random_divisor_fast (GEN X, unsigned long n, unsigned long d);
GEN curve_random_divisor (GEN X, unsigned long n, unsigned long d);
GEN curve_divisor_Frob(GEN X, GEN W_D, unsigned long m);

#endif
