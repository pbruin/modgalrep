#include <pari/pari.h>

#include "matsmall.h"

#include "addflip-chain.h"
#include "curve.h"
#include "divisor.h"
#include "jacobian.h"
#include "pairings.h"


static GEN
norm_of_section(GEN X, GEN s, GEN C1, GEN piv2, GEN R2) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN s_C1 = curve_multiply_section_subspace(X, s, C1);
  GEN M = matsmall_mul(R2, rowpermute(s_C1, piv2), p, T);
  return matsmall_det(M, p, T);
}

static int
divisors_disjoint(GEN X, GEN W_D, GEN W_E) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  GEN W_F;
  W_D = curve_multiply_subspaces(X, W_D, curve_V(X, 1));
  W_E = curve_multiply_subspaces(X, W_E, curve_V(X, 1));
  W_F = matsmall_image(shallowconcat(W_D, W_E), p, T);
  return curve_divisor_degree(X, W_F, 3) == 0;
}

static int
all_disjoint(GEN X, GEN W_D, GEN W_E) {
  long i;
  for (i = 1; i < lg(W_E); i++) {
    if (!divisors_disjoint(X, W_D, gel(W_E, i)))
      return 0;
  }
  return 1;
}

static void
find_disjoint_divisors(GEN J, GEN W_D, GEN *W_E, GEN *u_D0,
		       GEN *W_D0, GEN *u_E0, GEN *W_E0) {
  while (!divisors_disjoint(J, W_D, *W_E))
    *W_E = jacobian_random_presentation(J, *W_E);

  do {
    *u_D0 = curve_random_section(J, curve_V(J, 1));
    *W_D0 = curve_multiply_section_subspace(J, *u_D0, curve_V(J, 1));
  } while (!divisors_disjoint(J, *W_D0, *W_E));
  do {
    *u_E0 = curve_random_section(J, curve_V(J, 1));
    *W_E0 = curve_multiply_section_subspace(J, *u_E0, curve_V(J, 1));
  } while (!divisors_disjoint(J, *W_E0, W_D)
	   || !divisors_disjoint(J, *W_E0, *W_D0));
}

/*
  Input:
    W_D, W_D0: subspaces defining divisors  D, D_0  such that
      {\cal L}(-D_0) is trivial and {\cal L}(-D) is of order  n
    c = [[i_2, j_2], ..., [i_m, j_m]]: addflip chain for  n

  Output:
    [D_0, D_1, ..., D_m]: divisors of degree  d
    [s_2, s_3, ..., s_m]: sections of  {\cal L}^3  with
      div(s_l) = D_{i_l} + D_{j_l} + D_l
    v: section of  {\cal L}  with  div(v) = D_m
*/
static void
torsion_data(GEN J, GEN W_D, GEN W_D0, GEN c,
	     GEN *D, GEN *s, GEN *v, GEN W_avoid) {
  long i, j, l, m = lg(c);
  int random = (W_avoid != NULL);

  *D = cgetg(m + 2, t_VEC);
  *s = cgetg(m, t_VEC);
  gel(*D, 1) = W_D0;
  gel(*D, 2) = W_D;
  for (l = 2; l <= m; l++) {
    i = mael(c, l - 1, 1);
    j = mael(c, l - 1, 2);
    do {
      gel(*D, l + 1) = jacobian_addflip_cert(J, gel(*D, i + 1), gel(*D, j + 1),
					     &gel(*s, l - 1), random);
    } while (random && !all_disjoint(J, gel(*D, l + 1), W_avoid));
  }
  if (!jacobian_is_zero_cert(J, gel(*D, m + 1), v))
    pari_err(e_MISC, "inconsistent: line bundle is non-trivial");
}

static GEN
eval_half(GEN X, GEN c, GEN u, GEN D, GEN s, GEN v, GEN W_E) {
  unsigned long p = curve_base_field_characteristic(X);
  GEN T = curve_base_field_polynomial(X);
  long i, j, l, m = lg(c);
  GEN u3, W2, C2, piv3, W3_E, R3, piv5, W5_E, R5, gamma, sigma, tau;

  W2 = curve_V(X, 2);
  C2 = matsmall_complement(W2, W_E, p, T);
  piv3 = curve_pivots_V(X, 3);
  W3_E = curve_multiply_subspaces(X, curve_V(X, 1), W_E);
  R3 = matsmall_coker(rowpermute(W3_E, piv3), p, T);
  piv5 = curve_pivots_V(X, 5);
  W5_E = curve_multiply_subspaces(X, W2, W3_E);
  R5 = matsmall_coker(rowpermute(W5_E, piv5), p, T);
  u3 = curve_multiply_sections(X, u, curve_multiply_sections(X, u, u));
  tau = norm_of_section(X, u3, C2, piv5, R5);

  gamma = cgetg(m + 2, t_VEC);
  gel(gamma, 1) = gen_1;
  gel(gamma, 2) = gen_1;
  for (l = 2; l <= m; l++) {
    i = mael(c, l - 1, 1);
    j = mael(c, l - 1, 2);
    sigma = norm_of_section(X, gel(s, l - 1), C2, piv5, R5);
    gel(gamma, l + 1) = gdiv(tau, gmul(sigma, gmul(gel(gamma, i + 1),
						   gel(gamma, j + 1))));
  }
  return gmul(gel(gamma, m + 1),
	      gdiv(norm_of_section(X, v, C2, piv3, R3),
		   norm_of_section(X, u, C2, piv3, R3)));
}

static GEN
eval_function(GEN J, GEN c, GEN W_D, GEN W_D0, GEN u_D0,
	      GEN W_E, GEN W_E0) {
  GEN D, s, v;
  torsion_data(J, W_D, W_D0, c, &D, &s, &v, mkvec2(W_E, W_E0));
  return gdiv(eval_half(J, c, u_D0, D, s, v, W_E),
	      eval_half(J, c, u_D0, D, s, v, W_E0));
}

/*
  Compute the Tate-Lichtenbaum-Frey-Rück pairing: let  m  be a
  positive integer not divisible by the characteristic of the
  base field  k, and such that  k  contains the  m-th roots of
  unity.  Then there is a non-degenerate pairing

    J(k)[m] \times J(k)/mJ(k) ----> k^\times/k^{\times m}
              (x, y)         \mapsto      {x, y}_m

  which can be computed as described by Frey and Rück.  The points
  x  and  y  are represented as  {\cal L}(-D)  and  {\cal L}(-E).
  Because  x  is an  m-torsion point,  {\cal L}^m  has a global
  section  f  whose divisor equals  mD.  We take any rational
  section  g  of  {\cal L}(-E)  and check whether its divisor
  div(g)  is disjoint from  D.  If so, the result  {x, y}_m
  is equal  to  f(div(g))  (up to sign).
*/
GEN
jacobian_tate_pairing(GEN J, GEN W_D, GEN W_E, long n) {
  GEN q = curve_base_field_cardinality(J);
  GEN c, u_D0, W_D0, u_E0, W_E0, f_E;
  pari_sp av = avma;

  if (n <= 0 || smodis(q, n) != 1)
    pari_err(e_MISC, "base field does not contain "
	     "the roots of unity of order %li", n);

  if (jacobian_type(J) != JACOBIAN_TYPE_MEDIUM)
    pari_err_IMPL("jacobian_tate_pairing for non-medium models");

  c = addflip_chain(stoi(n));
  find_disjoint_divisors(J, W_D, &W_E, &u_D0, &W_D0, &u_E0, &W_E0);
  f_E = eval_function(J, c, W_D, W_D0, u_D0, W_E, W_E0);
  return gerepileupto(av, powgi(f_E, diviuexact(subis(q, 1), n)));
}

GEN
jacobian_weil_pairing(GEN J, GEN W_D, GEN W_E, long n) {
  GEN c, u_D0, W_D0, u_E0, W_E0, f_E, g_D;
  pari_sp av = avma;

  if (jacobian_type(J) != JACOBIAN_TYPE_MEDIUM)
    pari_err_IMPL("jacobian_weil_pairing for non-medium models");

  c = addflip_chain(stoi(n));
  find_disjoint_divisors(J, W_D, &W_E, &u_D0, &W_D0, &u_E0, &W_E0);
  f_E = eval_function(J, c, W_D, W_D0, u_D0, W_E, W_E0);
  g_D = eval_function(J, c, W_E, W_E0, u_E0, W_D, W_D0);
  return gerepileupto(av, gdiv(f_E, g_D));
}
