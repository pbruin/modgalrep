/*
  A space of cusp forms is represented as  [M, map]  where  M
  is a space of modular forms and  map  the matrix of the inclusion map
  with respect to some basis of the dual space of cusp forms
  and the chosen basis of  M .
*/

#define cusp_forms_modular_symbols(S) gel(S, 1)
#define cusp_forms_group(S) modular_symbols_group(gel(S, 1))
#define cusp_forms_weight(S) modular_symbols_weight(gel(S, 1))
#define cusp_forms_to_modular_symbols(S) gel(S, 2)
#define cusp_forms_dimension(S) (lg(gel(S, 2)) - 1)

GEN cusp_forms(GEN group, long weight, GEN p);
GEN cusp_forms_diamond_operator(GEN group, long d);
GEN cusp_forms_hecke_operator(GEN group, long n);
GEN cusp_forms_hecke_algebra(GEN S, long bound);
GEN cusp_forms_atkin_lehner_operator(GEN S, long d);
GEN cusp_forms_q_expansion_basis(GEN S, long nterms);
GEN hecke_algebra_q_expansion_basis (GEN group, long weight, GEN alg);
GEN cusp_forms_as_hecke_module(GEN group, long weight, GEN p, long nterms);
