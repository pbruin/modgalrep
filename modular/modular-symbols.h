#ifndef MODULAR_SYMBOLS
#define MODULAR_SYMBOLS

/*
  A space of modular symbols is represented as
  [group, weight, manin_symbols, basis,
   manin_symbols_to_basis, boundary_map, cuspidal_subspace].
*/

#define modular_symbols_group(M) gel(M, 1)
#define modular_symbols_weight(M) itos(gel(M, 2))
#define modular_symbols_manin_symbols(M) gel(M, 3)
#define modular_symbols_basis(M) gel(M, 4)
#define modular_symbols_dimension(M) (lg(gel(M, 4)) - 1)
#define modular_symbols_manin_symbols_to_basis(M) gel(M, 5)
#define modular_symbols_boundary_map(M) gel(M, 6)
#define modular_symbols_cuspidal_subspace(M) gel(M, 7)

GEN modular_symbols(GEN group, long weight, GEN p);
ulong modular_symbols_characteristic(GEN M);
GEN modular_symbols_diamond_operator(GEN M, long d);
GEN heilbronn_cremona(long p);
GEN modular_symbols_hecke_operator(GEN M, long n);
GEN modular_symbols_star_operator(GEN M);
GEN modular_symbols_atkin_lehner_operator(GEN M, long d);
GEN modular_symbols_diamond_operator_cuspidal_part(GEN M, long d);
GEN modular_symbols_hecke_operator_cuspidal_part(GEN M, long n);
GEN modular_symbols_star_operator_cuspidal_part(GEN M);
GEN modular_symbols_atkin_lehner_operator_cuspidal_part(GEN M, long d);
GEN modular_symbols_cuspidal_plus_subspace(GEN M);

#endif
