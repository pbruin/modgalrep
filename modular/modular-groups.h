/***

    Modular groups
    
***/


/*
  A modular group is represented as

  [type, level, representatives, cusps, e_2, e_3,
   rep_table, cusp_table].
*/

#define modular_group_type(G) itos(gel(G, 1))
#define modular_group_representatives(G) gel(G, 3)
#define modular_group_degree(G) (lg(gel(G, 3)) - 1)
#define modular_group_cusps(G) gel(G, 4)
#define modular_group_num_cusps(G) (lg(gel(G, 4)) - 1)
#define modular_group_repr_table(G) gel(G, 7)
#define modular_group_cusp_table(G) gel(G, 8)

#define GAMMA_0 0
#define GAMMA_1 1
#define pmGAMMA_1 2
#define GAMMA_H 3

static inline long
modular_group_level(GEN G) {
  return itos((modular_group_type(G) == GAMMA_H) ? znstar_get_N(gmael(G, 2, 1))
	      : gel(G, 2));
}

static inline GEN
modular_group_znstar(GEN G) {
  if (modular_group_type(G) != GAMMA_H)
    pari_err(e_MISC, "group not of type Gamma_H");
  return gmael(G, 2, 1);
}

static inline GEN
modular_group_H(GEN G) {
  if (modular_group_type(G) != GAMMA_H)
    pari_err(e_MISC, "group not of type Gamma_H");
  return gmael(G, 2, 2);
}

long modular_group_contains_minus1(GEN G);
long modular_group_num_elliptic_points_2(GEN G);
long modular_group_num_elliptic_points_3(GEN G);
long modular_group_num_regular_cusps(GEN G);
long modular_group_num_irregular_cusps(GEN G);
long modular_group_genus(GEN G);
GEN modular_group_modular_forms_degree_fine(GEN G, long weight);
GEN modular_group_cusp_forms_degree_fine(GEN G, long weight);
long modular_group_modular_forms_degree_coarse(GEN G, long weight);
long modular_group_cusp_forms_degree_coarse(GEN G, long weight);

GEN Gamma_0(long n);
GEN Gamma_1(long n);
GEN pmGamma_1(long n);
GEN Gamma_H(GEN Z, GEN H);

/*** Looking up cusps and representatives ***/

/* cusp must be given as a Vecsmall([u, v]).  */
inline static long
modular_group_cusp_index(GEN group, GEN cusp) {
  long n, u, v;

  n = modular_group_level(group);
  u = smodss(cusp[1], n);
  v = smodss(cusp[2], n);
  return itos(gcoeff(modular_group_cusp_table(group), u + 1, v + 1));
}

/* rep must be given as a Vecsmall([c, d]).  */
inline static long
modular_group_repr_index(GEN group, GEN rep) {
  long n, c, d;

  n = modular_group_level(group);
  c = smodss(rep[1], n);
  d = smodss(rep[2], n);
  return itos(gcoeff(modular_group_repr_table(group), c + 1, d + 1));
}
