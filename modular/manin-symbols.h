GEN manin_symbols(GEN group, long weight);
long manin_symbol_index(GEN group, long weight, GEN sym);
GEN manin_symbols_basis(GEN group, long weight, GEN syms, GEN p);
GEN boundary_map_on_manin_symbol(GEN group, long weight, GEN sym);
