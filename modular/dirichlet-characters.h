/***

    Dirichlet characters

***/

/*
  A Dirichlet character modulo n is represented as
    chi = [conductor, values_on_generators, values],
  where values is a vector of length n containing 
  The knowledge of values_on_generators is only useful
  when the canonical generators of (Z/nZ)^\times are known.

  The group of Dirichlet characters modulo n is represented as
    G = [n, gen, exponent, [chi_1, ..., chi_t]],
  where t = eulerphi(n).
*/

#define char_conductor(chi) itos(gel(chi, 1))
#define char_modulus(chi) (lg(gel(chi, 3)) - 1)
#define char_evaluate(chi, a) gmael(chi, 3, smodss(a, char_modulus(chi)) + 1)

#define dirichlet_group_modulus(G) gel(G, 1)
#define dirichlet_group_gen(G) gel(G, 2)
#define dirichlet_group_exponent(G) gel(G, 3)
#define dirichlet_group_characters(G) gel(G, 4)
#define dirichlet_group_order(G) (lg(gel(G, 4)) - 1)

GEN char_multiply(GEN G, GEN chi1, GEN chi2);
GEN char_divide(GEN G, GEN chi1, GEN chi2);
GEN char_apply_galois(GEN chi, long a);
GEN char_invert(GEN chi);
GEN char_make_primitive(GEN chi);
GEN dirichlet_group(long n, GEN z);
